package microservice.dungeon.game.config

import org.apache.kafka.clients.admin.NewTopic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class KafkaTopicConfig @Autowired constructor(
    @Value("\${kafka.topicProdRound}")
    private val topicProdRound: String,
    @Value("\${kafka.topicProdGame}")
    private val topicProdGame: String,
    @Value("\${kafka.topicProdPlayer}")
    private val topicProdPlayer: String
) {
    @Bean
    fun prodRoundTopic(): NewTopic = NewTopic(topicProdRound, 1, 1)

    @Bean
    fun prodGameTopic(): NewTopic = NewTopic(topicProdGame, 1, 1)

    @Bean
    fun prodPlayerTopic(): NewTopic = NewTopic(topicProdPlayer, 1, 1)
}