package microservice.dungeon.game.aggregates.core

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.validation.ConstraintViolationException

@ControllerAdvice
class GlobalExceptionHandler {
    @ExceptionHandler(Exception::class)
    fun handleException(e: Exception): ResponseEntity<ErrorDetails> {
        val errorDetails = ErrorDetails(
            e.message ?: "Unknown error",
            null,
            // We return the full stack trace on unknown errors to make debugging easier.
            e.stackTraceToString()
        )
        return ResponseEntity.internalServerError().body(errorDetails)
    }

    @ExceptionHandler(IllegalArgumentException::class)
    fun handleException(e: IllegalArgumentException): ResponseEntity<ErrorDetails> {
        val errorDetails = ErrorDetails(
            e.message ?: "An illegal argument was provided",
            null,
            // We return the full stack trace on unknown errors to make debugging easier.
            e.stackTraceToString()
        )
        return ResponseEntity.badRequest().body(errorDetails)
    }

    @ExceptionHandler(ConstraintViolationException::class)
    fun handleException(e: ConstraintViolationException): ResponseEntity<ErrorDetails> {
        val errorDetails = ErrorDetails(
            e.message ?: "A constraint violation occurred",
            e.constraintViolations.joinToString(",") { it.message },
            // We return the full stack trace on unknown errors to make debugging easier.
            e.stackTraceToString()
        )
        return ResponseEntity.badRequest().body(errorDetails)
    }
}
