package microservice.dungeon.game.aggregates.command.domain

class CommandArgumentException(override val message: String) : Exception(message) {
}