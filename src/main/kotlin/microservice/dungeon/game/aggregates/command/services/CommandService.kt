package microservice.dungeon.game.aggregates.command.services

import microservice.dungeon.game.aggregates.command.controller.dto.CommandRequestDto
import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.domain.CommandArgumentException
import microservice.dungeon.game.aggregates.command.domain.CommandType
import microservice.dungeon.game.aggregates.command.repositories.CommandRepository
import microservice.dungeon.game.aggregates.command.web.RobotClient
import microservice.dungeon.game.aggregates.command.web.TradingClient
import microservice.dungeon.game.aggregates.command.web.dto.BuyCommandDto
import microservice.dungeon.game.aggregates.command.web.dto.SellCommandDto
import microservice.dungeon.game.aggregates.game.domain.*
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.player.domain.PlayerNotFoundException
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class CommandService @Autowired constructor(
    private val commandRepository: CommandRepository,
    private val robotRepository: RobotRepository,
    private val gameRepository: GameRepository,
    private val playerRepository: PlayerRepository,
    private val robotClient: RobotClient,
    private val tradingClient: TradingClient
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @Transactional(rollbackOn = [Exception::class])
    @Throws(PlayerNotFoundException::class, GameNotFoundException::class, GameStateException::class, CommandArgumentException::class)
    fun createNewCommand(gameId: UUID, playerId: UUID, robotId: UUID?, commandType: CommandType, commandRequestDTO: CommandRequestDto): UUID {
        val player = playerRepository.findById(playerId)
            .orElseThrow {
                // Notice that we rely on the player ID to be confident as some kind of authentication
                // mechanism. However, we can safely log the ID for debugging, as obviously no
                // player with that ID exists.
                PlayerNotFoundException("Player with ID $playerId not found.")
            }
        val game: Game = gameRepository.findById(gameId)
            .orElseThrow { GameNotFoundException("Game with ID $gameId not found.") }
        val round: Round = game.getCurrentRound() ?: throw GameStateException("Game not in a state to accept commands. [gameStatus=${game.getGameStatus()}]")

        if (!game.isParticipating(player)) {
            throw GameParticipationException("Player '${player.getUserName()}' is not participating in the game.")
        }

        // This is some 200IQ Optional and nullability handling.
        // I don't know how to write this in a more readable and secure way without bringing in new
        // abstractions. I think for the time being we can live with that.
        val robot = if(robotId == null) null else robotRepository.findById(robotId).orElse(null)

        val newCommand = Command.makeCommandFromDto(
            round = round,
            player = player,
            robot = robot,
            commandType = commandType,
            dto = commandRequestDTO
        )
        val transactionId = newCommand.getCommandId()

        commandRepository.deleteCommandsByRoundAndPlayerAndRobot(round.getRoundId(), player.getPlayerId(), robot?.getRobotId())

        commandRepository.save(newCommand)
        logger.debug("New Command successfully created. [playerName={}, commandType={}, roundNumber={}]",
            player.getUserName(), commandType, round.getRoundNumber())

        return transactionId
    }

    @Transactional(rollbackOn = [Exception::class])
    fun dispatchCommands(roundId: UUID, type: CommandType) {
        val commands: List<Command> = commandRepository.findAllCommandsByRoundAndCommandType(roundId, type)

        when (type) {
            CommandType.SELLING -> tradingClient.sendSellingCommands(commands.map { SellCommandDto.makeFromCommand(it) })
            CommandType.BUYING -> tradingClient.sendBuyingCommands(commands.map { BuyCommandDto.makeFromCommand(it) })
            CommandType.MINING, CommandType.MOVEMENT, CommandType.BATTLE, CommandType.REGENERATE -> robotClient.sendCommands(commands)
        }
    }
}
