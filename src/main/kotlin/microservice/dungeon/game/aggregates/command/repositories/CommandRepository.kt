package microservice.dungeon.game.aggregates.command.repositories

import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.domain.CommandType
import microservice.dungeon.game.aggregates.game.domain.Round
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CommandRepository : JpaRepository<Command, UUID> {

    @Query("SELECT c FROM Command c join c.round r where r.roundId = ?1 and c.commandType = ?2")
    fun findAllCommandsByRoundAndCommandType(round: UUID, commandType: CommandType): List<Command>

    fun findAllCommandsByRound(round: Round): List<Command>

    @Modifying
    @Query(
        """
        delete from Command c 
          where c.round.roundId = ?1 
          and c.player.playerId = ?2
          and c.robot.robotId = ?3
         """
    )
    fun deleteCommandsByRoundAndPlayerAndRobot(round: UUID, player: UUID, robot: UUID?)

    fun findAllByRoundGameGameIdAndRoundRoundNumber(gameId: UUID, roundNumber: Int): List<Command>
}