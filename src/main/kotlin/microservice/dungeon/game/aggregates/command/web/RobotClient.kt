package microservice.dungeon.game.aggregates.command.web

import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.web.dto.RobotCommandDto
import microservice.dungeon.game.aggregates.command.web.dto.RobotCommandWrapperDto
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient

@Component
class RobotClient @Autowired constructor(
    @Value(value = "\${rest.robot.baseurl}")
    private val robotBaseURL: String
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val webClient = WebClient.create( if(robotBaseURL.startsWith("http://")) robotBaseURL else "http://$robotBaseURL")

    fun sendCommands(commands: List<Command>) {
        if (commands.isEmpty()) {
            logger.trace("No Commands to dispatch.")
            return
        }
        return try {
            logger.trace("Starting to dispatch {} Commands to RobotService ...", commands.size)
            transmitCommandsToRobot(
                RobotCommandWrapperDto(
                    commands.map { RobotCommandDto.makeFromCommand(it) }
                )
            )
            logger.trace("... dispatching of Commands completed.")

        } catch (e: Exception) {
            logger.error(e) { "Failed to dispatch Commands!" }
        }
    }
    private fun transmitCommandsToRobot(wrappedCommands: RobotCommandWrapperDto) {
        logger.trace("Robot-Endpoint is: POST ${robotBaseURL}/commands")
        logger.trace(ObjectMapper().writeValueAsString(wrappedCommands))

        webClient.post().uri("/commands")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(wrappedCommands)
            .exchangeToMono{ clientResponse ->
                if (clientResponse.statusCode() == HttpStatus.ACCEPTED) {
                    clientResponse.bodyToMono(String::class.java)
                }
                else {
                    throw Exception("Connection failed w/ status-code: ${clientResponse.statusCode()}")
                }
            }.block()
    }
}
