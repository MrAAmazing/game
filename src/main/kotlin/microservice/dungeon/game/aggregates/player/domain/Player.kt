package microservice.dungeon.game.aggregates.player.domain

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.*

@Entity
@Table(
    name = "players",
    uniqueConstraints = [
        UniqueConstraint(name = "player_unique_userName", columnNames = ["user_name"]),
        UniqueConstraint(name = "player_unique_mailAddress", columnNames = ["mail_address"])
])
class Player constructor(
    @Id
    @Column(name="player_id")
    @Type(type="uuid-char")
    private var playerId: UUID,

    @Column(name="user_name")
    private var userName: String,

    @Column(name="mail_address")
    private var mailAddress: String,
) {

    constructor(userName: String, mailAddress: String): this(
        UUID.randomUUID(), userName, mailAddress
    )

    fun getPlayerId(): UUID = playerId

    fun getUserName(): String = userName

    fun getMailAddress(): String = mailAddress

    override fun toString(): String {
        return "Player(playerId=$playerId, userName='$userName', mailAddress='${"XXX"}')"
    }
}
