package microservice.dungeon.game.aggregates.player.domain

class PlayerNotFoundException(override val message: String) : Exception(message) {
}