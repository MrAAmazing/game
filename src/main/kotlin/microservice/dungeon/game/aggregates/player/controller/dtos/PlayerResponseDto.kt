package microservice.dungeon.game.aggregates.player.controller.dtos

import microservice.dungeon.game.aggregates.player.domain.Player
import java.util.*

class PlayerResponseDto(
    val playerId: UUID,
    val name: String,
    val email: String
) {
    companion object {
        fun makeFromPlayer(player: Player): PlayerResponseDto =
            PlayerResponseDto(
                player.getPlayerId(), player.getUserName(), player.getMailAddress()
            )
    }

    override fun toString(): String {
        return "PlayerResponseDto(playerId=$playerId, name='$name', email='$email')"
    }
}
