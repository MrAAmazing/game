package microservice.dungeon.game.aggregates.game.controller.dto

import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*

class CreateGameResponseDto (
    val gameId: UUID
) {

    override fun toString(): String =
        "CreateGameResponseDto(gameId=$gameId)"
}