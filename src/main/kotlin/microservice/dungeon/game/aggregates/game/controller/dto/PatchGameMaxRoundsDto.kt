package microservice.dungeon.game.aggregates.game.controller.dto

import javax.validation.constraints.Min

class PatchGameMaxRoundsDto (
    @Min(1) val maxRounds: Int
)