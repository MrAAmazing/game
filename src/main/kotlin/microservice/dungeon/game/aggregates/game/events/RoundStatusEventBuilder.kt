package microservice.dungeon.game.aggregates.game.events

import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.core.Event
import microservice.dungeon.game.aggregates.core.EventBuilder
import microservice.dungeon.game.aggregates.game.domain.RoundStatus
import microservice.dungeon.game.aggregates.game.services.GameLoopTimingPredictions
import microservice.dungeon.game.aggregates.game.services.GameLoopTimings
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class RoundStatusEventBuilder @Autowired constructor(
    @Value("\${kafka.event.prod.roundStatus.topic}")
    private val topic: String,
    @Value("\${kafka.event.prod.roundStatus.type}")
    private val eventType: String,
    @Value("\${kafka.event.prod.roundStatus.version}")
    private val version: Int

) : EventBuilder {

    companion object {
        val objectMapper: ObjectMapper = ObjectMapper().findAndRegisterModules()
    }

    override fun deserializedEvent(serialized: String): Event {
        return objectMapper.readValue(serialized, RoundStatusEvent::class.java)
    }

    fun makeRoundStatusEvent(
        transactionId: UUID,
        gameId: UUID,
        roundId: UUID,
        roundNumber: Int,
        roundStatus: RoundStatus,
        timings: GameLoopTimings,
        predictions: GameLoopTimingPredictions?
    ): RoundStatusEvent {
        return RoundStatusEvent(
            id = UUID.randomUUID(),
            transactionId = transactionId,
            occurredAt = Instant.now(),
            eventName = eventType,
            topic = topic,
            version = version,
            gameId = gameId,
            roundId = roundId,
            roundNumber = roundNumber,
            roundStatus = roundStatus,
            timings = timings,
            timingPredictions = predictions
        )
    }
}