package microservice.dungeon.game.aggregates.game.controller

import microservice.dungeon.game.aggregates.core.ErrorDetails
import microservice.dungeon.game.aggregates.game.controller.dto.*
import microservice.dungeon.game.aggregates.game.domain.*
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.game.services.GameLoopService
import microservice.dungeon.game.aggregates.game.services.GameService
import microservice.dungeon.game.aggregates.player.domain.PlayerNotFoundException
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.transaction.Transactional
import javax.validation.Valid


@RestController
class GameController @Autowired constructor(
    private val gameService: GameService,
    private val gameRepository: GameRepository,
    private val gameLoopService: GameLoopService
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @ExceptionHandler(value = [GameAlreadyActiveException::class])
    fun handleException(e: GameAlreadyActiveException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorDetails(e.message, "Either end the game or wait for it to end."))
    }

    @ExceptionHandler(value = [GameNotFoundException::class])
    fun handleException(e: GameNotFoundException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorDetails(e.message, "Have you already created the game?"))
    }

    @ExceptionHandler(value = [PlayerNotFoundException::class])
    fun handleException(e: PlayerNotFoundException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorDetails(e.message, "Have you already created the player?"))
    }

    @ExceptionHandler(value = [GameStateException::class])
    fun handleException(e: GameStateException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorDetails(e.message, "Verify that the game is in the correct state."))
    }

    @ExceptionHandler(value = [GameParticipationException::class])
    fun handleException(e: GameParticipationException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ErrorDetails(e.message, "Verify participation of player."))
    }

    @ExceptionHandler(value = [NoActiveGameClockException::class])
    fun handleException(e: NoActiveGameClockException): ResponseEntity<ErrorDetails> {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorDetails(e.message, null , e.stackTraceToString()))
    }

    @PostMapping("/games", consumes = ["application/json"], produces = ["application/json"])
    fun createNewGame(@RequestBody @Valid requestGame: CreateGameRequestDto): ResponseEntity<CreateGameResponseDto> {
        val response: Pair<UUID, Game> = gameService
            .createNewGame(requestGame.maxPlayers, requestGame.maxRounds)
        val responseDto = CreateGameResponseDto(response.second.getGameId())

        logger.trace(
            "Request to create new Game was successful. [gameId={}]",
            response.second.getGameId()
        )
        return ResponseEntity(responseDto, HttpStatus.CREATED)
    }

    @PostMapping("/games/{gameId}/gameCommands/start")
    fun startGame(@PathVariable(name = "gameId") gameId: UUID): ResponseEntity<HttpStatus> {
        gameLoopService.start(gameId)
        logger.trace("Request to start Game was successful. [gameId={}]", gameId)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PostMapping("/games/{gameId}/gameCommands/end")
    fun endGame(@PathVariable(name = "gameId") gameId: UUID): ResponseEntity<HttpStatus> {
        gameLoopService.end(gameId)
        logger.debug("Request to end game was successful. [gameId={}]", gameId)
        return ResponseEntity(HttpStatus.CREATED)
    }

    @PutMapping("/games/{gameId}/players/{playerId}", produces = ["application/json"])
    fun joinGame(
        @PathVariable(name = "gameId") gameId: UUID,
        @PathVariable(name = "playerId") playerId: UUID
    ): ResponseEntity<JoinGameResponseDto> {
        val responseDto = gameService.joinGame(playerId, gameId)

        logger.debug("Request to join game was successful. [gameId={}]", gameId)
        return ResponseEntity(responseDto, HttpStatus.OK)
    }

    @Transactional(rollbackOn = [Exception::class])
    @GetMapping("/games/{gameId}/time", produces = ["application/json"])
    fun getGameTime(@PathVariable(name = "gameId") gameId: UUID): ResponseEntity<GameTimeResponseDto> {
        val game: Game = gameRepository.findById(gameId)
            .orElseThrow { GameNotFoundException("Game with ID '${gameId}' not found.") }
        val responseBody = GameTimeResponseDto(game)

        return ResponseEntity(responseBody, HttpStatus.OK)
    }

    @Transactional(rollbackOn = [Exception::class])
    @GetMapping("/games", produces = ["application/json"])
    fun getGames(): List<GameResponseDto> {
        return gameRepository.findAllByGameStatusIn(
            listOf(
                GameStatus.CREATED,
                GameStatus.GAME_RUNNING
            )
        )
            .map { game -> GameResponseDto(game) }
    }

    @PatchMapping("/games/{gameId}/maxRounds")
    fun patchMaximumNumberOfRounds(
        @PathVariable(name = "gameId") gameId: UUID,
        @RequestBody @Valid dto: PatchGameMaxRoundsDto
    ): ResponseEntity<HttpStatus> {
        gameService.changeMaximumNumberOfRounds(gameId, dto.maxRounds)
        return ResponseEntity(HttpStatus.OK)
    }

    @PatchMapping("/games/{gameId}/duration")
    fun patchRoundDuration(
        @RequestBody @Valid dto: PatchGameDurationDto,
        @PathVariable(name = "gameId") gameId: UUID
    ): ResponseEntity<HttpStatus> {
        gameLoopService.changeRoundDuration(gameId, dto.duration)
        logger.debug("Request successful. Changed round duration to ${dto.duration} (in Millis).")
        logger.trace("Responding with 200.")
        return ResponseEntity(HttpStatus.OK)
    }
}
