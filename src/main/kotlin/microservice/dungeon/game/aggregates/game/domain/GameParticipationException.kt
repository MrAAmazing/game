package microservice.dungeon.game.aggregates.game.domain

class GameParticipationException(override val message: String) : Exception(message) {
}