package microservice.dungeon.game.aggregates.game.services

import microservice.dungeon.game.aggregates.eventpublisher.EventPublisher
import microservice.dungeon.game.aggregates.game.controller.dto.JoinGameResponseDto
import microservice.dungeon.game.aggregates.game.domain.*
import microservice.dungeon.game.aggregates.game.events.*
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.player.domain.PlayerNotFoundException
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.player.services.PlayerQueueManager
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional


@Service
class GameService @Autowired constructor(
    private val gameRepository: GameRepository,
    private val playerRepository: PlayerRepository,
    private val eventPublisher: EventPublisher,
    private val gameStatusEventBuilder: GameStatusEventBuilder,
    private val playerStatusEventBuilder: PlayerStatusEventBuilder,
    private val playerQueueManager: PlayerQueueManager
) {
    private val logger = KotlinLogging.logger {}

    @Transactional(rollbackOn = [Exception::class])
    @Throws(GameAlreadyActiveException::class)
    fun createNewGame(maxPlayers: Int, maxRounds: Int): Pair<UUID, Game> {
        if (gameRepository.existsByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING))) {
            throw GameAlreadyActiveException("A new Game could not be created, because an active Game already exists.")
        }

        val transactionId = UUID.randomUUID()
        val newGame = Game(maxPlayers, maxRounds)

        gameRepository.save(newGame)
        logger.info("New Game created. [transactionId=$transactionId]")
        logger.trace(newGame.toString())

        val gameCreatedEvent: GameStatusEvent = gameStatusEventBuilder.makeGameStatusEvent(transactionId, newGame.getGameId(), GameStatus.CREATED)
        eventPublisher.publishEvent(gameCreatedEvent)

        this.playerQueueManager.setupGameExchange()

        return Pair(transactionId, newGame)
    }

    @Transactional(rollbackOn = [Exception::class])
    @Throws(GameStateException::class, GameNotFoundException::class, PlayerNotFoundException::class)
    fun joinGame(playerId: UUID, gameId: UUID): JoinGameResponseDto {
        val transactionId = UUID.randomUUID()

        val player = playerRepository.findById(playerId)
            .orElseThrow { PlayerNotFoundException("Failed to join game. Player with ID '$playerId' not found.") }
        val game = gameRepository.findById(gameId)
            .orElseThrow { GameNotFoundException("Failed to join game. Game with ID '$gameId' not found.") }

        game.joinGame(player)
        gameRepository.save(game)
        logger.info("Player has joined the game. [playerName=${player.getUserName()}, numberOfPlayers=${game.getParticipatingPlayers().size}/${game.getMaxPlayers()}]")

        val playerBinding = playerQueueManager.setupPlayerQueue(playerId)

        val playerJoinedEvent: PlayerStatusEvent = playerStatusEventBuilder.makePlayerStatusEvent(transactionId, player.getPlayerId(), game.getGameId(), player.getUserName())
        eventPublisher.publishEvent(playerJoinedEvent)

        return JoinGameResponseDto(playerBinding.exchange, playerBinding.destination)
    }

    @Transactional(rollbackOn = [Exception::class])
    @Throws(GameNotFoundException::class, GameStateException::class, IllegalArgumentException::class)
    fun changeMaximumNumberOfRounds(gameId: UUID, maxRounds: Int): UUID {
        val transactionId = UUID.randomUUID()
        val game = gameRepository.findById(gameId)
            .orElseThrow { GameNotFoundException("Game with ID '$gameId' not found.") }

        game.changeMaximumNumberOfRounds(maxRounds)
        gameRepository.save(game)

        logger.trace("Updated maximum number of rounds. [maxRounds=$maxRounds, gameId=$gameId]")

        return transactionId
    }
}

