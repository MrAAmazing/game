package microservice.dungeon.game.aggregates.game.events

import com.fasterxml.jackson.annotation.JsonIgnore
import microservice.dungeon.game.aggregates.core.AbstractEvent
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.game.events.dto.GameStatusEventDto
import java.time.Instant
import java.util.*

class GameStatusEvent (
    id: UUID,
    transactionId: UUID,
    occurredAt: Instant,
    eventName: String,
    topic: String,
    version: Int,

    val gameId: UUID,
    val gameStatus: GameStatus

) : AbstractEvent(
    id = id,
    transactionId = transactionId,
    occurredAt = occurredAt,
    eventName = eventName,
    topic = topic,
    version = version
) {
    override fun toDTO(): GameStatusEventDto = GameStatusEventDto(gameId, gameStatus)
    @JsonIgnore
    override fun getPlayerId(): UUID? = null

    override fun equals(other: Any?): Boolean =
        (other is GameStatusEvent)
                && getId() == other.getId()
                && getTransactionId() == other.getTransactionId()
                && getOccurredAt() == other.getOccurredAt()
                && getEventName() == other.getEventName()
                && getTopic() == other.getTopic()
                && getVersion() == other.getVersion()
                && gameId == other.gameId
                && gameStatus == other.gameStatus
}
