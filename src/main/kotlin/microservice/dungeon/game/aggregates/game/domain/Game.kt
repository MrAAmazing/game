package microservice.dungeon.game.aggregates.game.domain

import microservice.dungeon.game.aggregates.player.domain.Player
import mu.KotlinLogging
import org.hibernate.annotations.Type
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import javax.persistence.*

@Entity
@Table(
    name = "games",
    indexes = [
        Index(name = "game_gameStatus", columnList = "game_status")
    ]
)
class Game constructor (
    @Id
    @Type(type="uuid-char")
    @Column(name = "game_id")
    private var gameId: UUID,

    @Column(name = "game_status")
    @Enumerated(EnumType.STRING)
    private var gameStatus: GameStatus,

    @Column(name = "max_players")
    private var maxPlayers: Int,

    @Column(name = "max_rounds")
    private var maxRounds: Int,

    @Column(name = "total_round_timespan")
    private var totalRoundTimespanInMS: Long,

    @Column(name = "relative_command_input_timespan")
    private var relativeCommandInputTimespanInPercent: Int,

    @ManyToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JoinTable(
        name = "game_participations",
        joinColumns = [JoinColumn(name = "game_id")],
        inverseJoinColumns = [JoinColumn(name = "round_id")]) // should be playerId
    private var participatingPlayers: MutableSet<Player>,

    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    private var rounds: MutableSet<Round>

) {
    constructor(maximumPlayers: Int, maximumRounds: Int): this(
        gameId = UUID.randomUUID(),
        gameStatus = GameStatus.CREATED,
        maxPlayers = maximumPlayers,
        maxRounds = maximumRounds,
        totalRoundTimespanInMS = 60000,
        relativeCommandInputTimespanInPercent = 75,
        participatingPlayers = mutableSetOf(),
        rounds = mutableSetOf()
    )

    companion object {
        private val logger = KotlinLogging.logger {}
    }


    fun startGame() {
        if (gameStatus != GameStatus.CREATED) {
            throw GameStateException("Game can not be started, because its status is other than CREATED. Status is $gameStatus.")
        }
        gameStatus = GameStatus.GAME_RUNNING

        val newRound = Round(game = this, roundNumber = 1)
        rounds.add(newRound)
    }

    fun startNewRound() {
        if (gameStatus != GameStatus.GAME_RUNNING) {
            throw GameStateException("Game could not start a new round, because its status is other than RUNNING. Status is $gameStatus")
        }
        val currentRound = getCurrentRound()!!
        val roundNumber = currentRound.getRoundNumber()

        if (roundNumber >= maxRounds) {
            throw GameStateException("Start new round aborted. Maximum number of rounds reached. [${roundNumber}/${maxRounds}]")
        }

        if (currentRound.getRoundStatus() != RoundStatus.ROUND_ENDED) {
            currentRound.endRound()
        }

        val nextRound = Round(game = this, roundNumber = roundNumber + 1)
        rounds.add(nextRound)
    }

    fun endRound() {
        if (gameStatus != GameStatus.GAME_RUNNING) {
            throw GameStateException("Game could not start a new round, because its status is other than RUNNING. Status is $gameStatus")
        }
        val currentRound = getCurrentRound()!!
        currentRound.endRound()
    }

    fun endCommandInputPhase() {
        if (gameStatus != GameStatus.GAME_RUNNING) {
            throw GameStateException("Game could not start a new round, because its status is other than RUNNING. Status is $gameStatus")
        }
        val currentRound = getCurrentRound()!!
        currentRound.endCommandInputPhase()
    }

    fun endGame() {
        if(gameStatus != GameStatus.GAME_RUNNING) {
            throw GameStateException("Game could not be ended, because its status is other than RUNNING. Status is $gameStatus")
        }
        gameStatus = GameStatus.GAME_FINISHED
    }

    fun joinGame(player: Player) {
        if (gameStatus != GameStatus.CREATED) {
            throw GameStateException("Failed to join Game. Games may only be joined when status is CREATED. Current status is $gameStatus")
        }

        if (participatingPlayers.map { it.getPlayerId() }.contains(player.getPlayerId())) {
            throw GameParticipationException("Failed to join Game. Player is already participating.")
        }

        if (participatingPlayers.size >= maxPlayers) {
            throw GameParticipationException("Failed to join Game. Game is already full. [currentPlayers=${participatingPlayers.size}, maxPlayers=$maxPlayers]")
        }

        participatingPlayers.add(player)
    }

    fun getGameId(): UUID = gameId

    fun getGameStatus(): GameStatus = gameStatus

    fun getMaxPlayers(): Int = maxPlayers

    fun getParticipatingPlayers(): List<Player> = participatingPlayers.toList()

    fun getNumberJoinedPlayers(): Int = participatingPlayers.size

    fun getMaxRounds(): Int = maxRounds

    fun getCurrentRound(): Round? = rounds.maxByOrNull { it.getRoundNumber() }

    fun getRound(roundNumber: Int): Round = rounds.first { round ->
        round.getRoundNumber() == roundNumber
    }

    fun getTotalRoundTimespanInMS(): Long = totalRoundTimespanInMS

    fun getRelativeCommandInputTimespanInPercent(): Int = relativeCommandInputTimespanInPercent

    fun getTimeGameStartedTruncatedToSeconds(): Instant? {
        return if (gameStatus == GameStatus.CREATED) {
            null
        } else {
            getRound(1).getRoundStarted()
        }
    }

    fun getTimeSinceGameStartInSeconds(): Long? {
        return if (gameStatus == GameStatus.CREATED) {
            null
        } else {
            ChronoUnit.SECONDS.between(getTimeGameStartedTruncatedToSeconds()!!, Instant.now())
        }
    }

    fun isParticipating(player: Player): Boolean {
        return participatingPlayers.asIterable().map { it.getPlayerId() }.contains(player.getPlayerId())
    }

    fun changeMaximumNumberOfRounds(maxRounds: Int) {
        if (gameStatus == GameStatus.GAME_FINISHED) {
            throw GameStateException("Game is already finished.")
        }
        if (maxRounds < 1) {
            throw IllegalArgumentException("Maximum number of rounds may never below 1.")
        }
        if (getCurrentRound() != null && maxRounds <= getCurrentRound()!!.getRoundNumber()) {
            throw IllegalArgumentException("Maximum number of rounds is below current round-number.")
        }
        this.maxRounds = maxRounds
    }

    fun changeRoundDuration(duration: Long) {
        if (gameStatus == GameStatus.GAME_FINISHED) {
            throw GameStateException("Game is already finished.")
        }
        if (duration < 2000) {
            throw IllegalArgumentException("Game duration may never be below 2 seconds. [durationInMillis=$duration]")
        }
        this.totalRoundTimespanInMS = duration
    }

    override fun toString(): String {
        return "Game(gameId=$gameId, gameStatus='$gameStatus', maxPlayers=$maxPlayers, maxRounds=$maxRounds, totalRoundTimespanInMS=$totalRoundTimespanInMS, relativeCommandInputTimespanInPercent=$relativeCommandInputTimespanInPercent, participatingPlayers=${participatingPlayers.map{it.getUserName()}}, currentRoundNumber=${getCurrentRound()?.getRoundNumber()})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Game

        if (gameId != other.gameId) return false
        if (gameStatus != other.gameStatus) return false
        if (maxPlayers != other.maxPlayers) return false
        if (maxRounds != other.maxRounds) return false
        if (totalRoundTimespanInMS != other.totalRoundTimespanInMS) return false
        if (relativeCommandInputTimespanInPercent != other.relativeCommandInputTimespanInPercent) return false
        if (participatingPlayers != other.participatingPlayers) return false
        if (rounds != other.rounds) return false

        return true
    }

    override fun hashCode(): Int {
        var result = gameId.hashCode()
        result = 31 * result + gameStatus.hashCode()
        result = 31 * result + maxPlayers
        result = 31 * result + maxRounds
        result = 31 * result + totalRoundTimespanInMS.hashCode()
        result = 31 * result + relativeCommandInputTimespanInPercent
        result = 31 * result + participatingPlayers.hashCode()
        result = 31 * result + rounds.hashCode()
        return result
    }


}