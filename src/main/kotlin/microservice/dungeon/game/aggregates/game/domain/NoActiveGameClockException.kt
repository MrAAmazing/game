package microservice.dungeon.game.aggregates.game.domain

class NoActiveGameClockException(override val message: String) : Exception(message) {
}