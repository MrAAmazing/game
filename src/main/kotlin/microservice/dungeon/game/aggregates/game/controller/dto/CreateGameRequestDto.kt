package microservice.dungeon.game.aggregates.game.controller.dto

import com.fasterxml.jackson.databind.ObjectMapper
import javax.validation.constraints.Min

class CreateGameRequestDto (
    @Min(1) val maxPlayers: Int,
    @Min(1) val maxRounds: Int
) {
    override fun toString(): String =
        "CreateGameRequestDto(maxPlayers=$maxPlayers, maxRounds=$maxRounds)"
}