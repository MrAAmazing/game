package microservice.dungeon.game.aggregates.game.services

import microservice.dungeon.game.aggregates.game.domain.TimeFrame
import mu.KotlinLogging
import java.time.Instant
import java.util.concurrent.atomic.AtomicBoolean

class GameLoopClock(
    private var timeFrame: TimeFrame,
) {
    private val logger = KotlinLogging.logger {}
    private val roundStartedListeners = mutableListOf<(GameLoopTimings) -> Unit>()
    private val commandInputEndedListeners = mutableListOf<(GameLoopTimings) -> Unit>()
    private val roundEndedListeners = mutableListOf<(GameLoopTimings) -> Unit>()
    private val running = AtomicBoolean(false)

    var predictions: GameLoopTimingPredictions? = null

    fun run() {
        logger.info { "Running game loop" }
        running.set(true)
        while (running.get()) {
            // We start a new round by getting all the time frames.
            // We need to to do this because it can be changed mid-round and we want a consistent
            // time frame for a whole round.
            val inputTime = timeFrame.commandInputTime()
            val executionTime = timeFrame.executionTime()
            val danglingTime = timeFrame.danglingTime()
            val timings = GameLoopTimings(Instant.now())
            updatePredictions(timings)

            // ---------
            // ROUND START
            if (!running.get()) {
                break
            }
            logger.trace { "Entered command acceptation phase." }

            roundStartedListeners.forEach { it(timings) }
            Thread.sleep(inputTime.toMillis())

            // ---------
            // COMMAND INPUT ENDED
            if (!running.get()) {
                break
            }
            logger.trace { "Entered command execution phase." }

            timings.commandInputEnded = Instant.now()
            updatePredictions(timings)
            commandInputEndedListeners.forEach { it(timings) }
            Thread.sleep(executionTime.toMillis())

            // ---------
            // ROUND END
            if (!running.get()) {
                break
            }
            logger.trace { "Entered end round phase. Don't wait" }

            timings.roundEnded = Instant.now()
            updatePredictions(timings)
            roundEndedListeners.forEach { it(timings) }
            Thread.sleep(danglingTime.toMillis())
        }
    }

    fun stop() {
        logger.info { "Stopping game loop" }
        running.set(false)
    }

    fun patchTimeFrame(timeFrame: TimeFrame) {
        this.timeFrame = timeFrame
    }

    fun onRoundStart(fn: (GameLoopTimings) -> Unit) {
        roundStartedListeners.add(fn)
    }
    fun onCommandInputEnded(fn: (GameLoopTimings) -> Unit) {
        commandInputEndedListeners.add(fn)
    }
    fun onRoundEnd(fn: (GameLoopTimings) -> Unit) {
        roundEndedListeners.add(fn)
    }

    private fun updatePredictions(timings: GameLoopTimings) {
        val inputTime = timeFrame.commandInputTime()
        val executionTime = timeFrame.executionTime()

        val predictedCommandInputEnd = timings.commandInputEnded ?: timings.roundStarted.plus(inputTime)
        val predictedRoundEnd = timings.roundEnded ?: predictedCommandInputEnd.plus(executionTime)

        predictions = GameLoopTimingPredictions(
            timings.roundStarted,
            predictedCommandInputEnd,
            predictedRoundEnd
        )
    }
}
