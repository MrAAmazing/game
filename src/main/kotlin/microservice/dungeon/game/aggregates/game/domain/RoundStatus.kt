package microservice.dungeon.game.aggregates.game.domain

enum class RoundStatus {
    COMMAND_INPUT_STARTED,
    COMMAND_INPUT_ENDED,
    ROUND_ENDED
}