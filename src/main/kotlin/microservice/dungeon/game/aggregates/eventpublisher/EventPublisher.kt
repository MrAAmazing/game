package microservice.dungeon.game.aggregates.eventpublisher

import microservice.dungeon.game.aggregates.core.Event
import microservice.dungeon.game.aggregates.core.KafkaProducing
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class EventPublisher @Autowired constructor(
    private val kafkaProducing: KafkaProducing
) {
    fun publishEvents(events: List<Event>) {
        events.forEach { event: Event ->
            kafkaProducing.send(event)
        }
    }

    fun publishEvent(event: Event) {
        kafkaProducing.send(event)
    }
}