package microservice.dungeon.game.aggregates.robot.consumer.dtos

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class RobotKilledDto(
    val robot: UUID
)
