package microservice.dungeon.game.aggregates.robot.consumer

import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.robot.services.RobotService
import microservice.dungeon.game.aggregates.robot.consumer.dtos.RobotSpawnedDto
import microservice.dungeon.game.aggregates.robot.consumer.dtos.RobotKilledDto
import mu.KotlinLogging
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component

@Component
class KafkaRobotConsumer @Autowired constructor(
    private val robotService: RobotService,
    private val objectMapper: ObjectMapper
) {
    private val logger = KotlinLogging.logger {}

    @KafkaListener(topics = ["robot"])
    fun robotListener(@Header("type") type: String, record: ConsumerRecord<String, String>) {
        when(type) {
            "RobotKilled" -> handleRobotKilled(record)
            "RobotSpawned" -> handleRobotSpawned(record)
            else -> logger.warn { "Unknown type: $type" }
        }
    }

    private fun handleRobotKilled(record: ConsumerRecord<String, String>) {
        val event = objectMapper.readValue(record.value(), RobotKilledDto::class.java)

        robotService.destroyRobot(event.robot)
    }

    private fun handleRobotSpawned(record: ConsumerRecord<String, String>) {
        val event = objectMapper.readValue(record.value(), RobotSpawnedDto::class.java)

        robotService.newRobot(event.robot.id, event.robot.player)
    }
}
