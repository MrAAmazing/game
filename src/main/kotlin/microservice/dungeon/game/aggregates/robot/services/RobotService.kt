package microservice.dungeon.game.aggregates.robot.services

import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.domain.Robot
import microservice.dungeon.game.aggregates.robot.domain.RobotAlreadyExistsException
import microservice.dungeon.game.aggregates.robot.domain.RobotNotFoundException
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class RobotService @Autowired constructor(
    private val robotRepository: RobotRepository
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    @Transactional(rollbackOn = [Exception::class])
    fun newRobot(robotId: UUID, playerId: UUID) {
        if (robotRepository.existsById(robotId)) {
            logger.warn("Failed to create new robot. Robot does already exist. [robotId=$robotId]")
            throw RobotAlreadyExistsException("Failed to create new robot. Robot does already exist.")
        }

        val newRobot = Robot(robotId, playerId)
        robotRepository.save(newRobot)
        logger.trace("Robot created. [robotId=${robotId}, playerId=${playerId}]")
    }

    @Transactional(rollbackOn = [Exception::class])
    fun destroyRobot(robotId: UUID) {
        val robot = robotRepository.findById(robotId)
            .orElseThrow { RobotNotFoundException("Failed to destroy robot. Robot not found.") }

        robot.destroyRobot()
        robotRepository.save(robot)
        logger.trace("Robot destroyed. [robotId=$robotId]")
    }
}
