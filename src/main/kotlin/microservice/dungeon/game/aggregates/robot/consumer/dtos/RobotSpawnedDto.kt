package microservice.dungeon.game.aggregates.robot.consumer.dtos

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class RobotSpawnedDto(
    val robot: RobotDto
) {
    @JsonIgnoreProperties(ignoreUnknown = true)
    data class RobotDto(
        val id: UUID,
        val player: UUID
    )
}
