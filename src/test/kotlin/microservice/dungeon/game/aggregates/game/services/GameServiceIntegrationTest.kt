package microservice.dungeon.game.aggregates.game.services

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.eventpublisher.EventPublisher
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameAlreadyActiveException
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.game.events.GameStatusEventBuilder
import microservice.dungeon.game.aggregates.game.events.PlayerStatusEventBuilder
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.player.services.PlayerQueueManager
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.groups.Tuple
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.mockito.ArgumentCaptor
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.Exchange
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.DirtiesContext
import java.util.*

@DirtiesContext
class GameServiceIntegrationTest @Autowired constructor(
    private val gameRepository: GameRepository,
    private val playerRepository: PlayerRepository,
    gameStatusEventBuilder: GameStatusEventBuilder,
    playerStatusEventBuilder: PlayerStatusEventBuilder
) : AbstractIntegrationTest() {
    private val mockEventPublisher: EventPublisher = mock()
    private val rabbitAdmin: RabbitAdmin = mock()
    private val playerQueueManager = PlayerQueueManager(rabbitAdmin)

    private val gameService: GameService = GameService(
        gameRepository,
        playerRepository,
        mockEventPublisher,
        gameStatusEventBuilder,
        playerStatusEventBuilder,
        playerQueueManager
    )

    @BeforeEach
    @AfterEach
    fun cleanup() {
        gameRepository.deleteAll()
        playerRepository.deleteAll()
    }

    @Test
    @Disabled
    fun shouldPersistGameWhenCreatingNewGame() {
        // given
        val maxPlayer: Int = 3
        val maxRounds: Int = 3

        // when
        val response: Pair<UUID, Game> = gameService.createNewGame(maxPlayer, maxRounds)

        // then
        val capturedGame = gameRepository.findById(response.second.getGameId()).get()
    }

    @Test
    fun shouldDeclareExchangeWhenCreatingNewGame() {
        // given
        val maxPlayer: Int = 3
        val maxRounds: Int = 3

        // when
        val response: Pair<UUID, Game> = gameService.createNewGame(maxPlayer, maxRounds)

        // then
        val captor = ArgumentCaptor.forClass(Exchange::class.java)
        verify(rabbitAdmin).declareExchange(captor.capture())
        assertThat(captor.value.name).isEqualTo("game")
    }

    @ParameterizedTest
    @EnumSource(
        value = GameStatus::class,
        names = ["GAME_FINISHED"],
        mode = EnumSource.Mode.EXCLUDE
    )
    fun shouldThrowWhenActiveGameAlreadyExists(activeStatus: GameStatus) {
        // given
        val activeGame: Game = Game(UUID.randomUUID(), activeStatus, 1, 1, 60000, 75, mutableSetOf(), mutableSetOf())
        gameRepository.save(activeGame)

        // when then
        assertThrows(GameAlreadyActiveException::class.java) {
            gameService.createNewGame(4, 4)
        }
    }

    @Test
    fun shouldPersistPlayerInGameWhenPlayerJoinsGame() {
        // given
        val player: Player = Player("dadepu", "any_mail")
        val game: Game = Game(1,1)

        playerRepository.save(player)
        gameRepository.save(game)

        // when
        gameService.joinGame(player.getPlayerId(), game.getGameId())

        // then
        val capturedGame: Game = gameRepository.findById(game.getGameId()).get()
        val participatingPlayers: List<Player> = capturedGame.getParticipatingPlayers().toList()
        assertThat(participatingPlayers[0].getPlayerId())
            .isEqualTo(player.getPlayerId())
    }

    @Test
    fun shouldDeclareQueueWhenPlayerJoinsGame() {
        // given
        val player: Player = Player("dadepu", "any_mail")
        val game: Game = Game(1,1)

        playerRepository.save(player)
        gameRepository.save(game)

        // when
        gameService.joinGame(player.getPlayerId(), game.getGameId())

        // then
        val captor = ArgumentCaptor.forClass(Queue::class.java)
        verify(rabbitAdmin).declareQueue(captor.capture())
        assertThat(captor.value.name).isEqualTo("player-${player.getPlayerId()}")
    }

    @Test
    fun shouldDeclareBindingWhenPlayerJoinsGame() {
        // given
        val player: Player = Player("dadepu", "any_mail")
        val game: Game = Game(1,1)

        playerRepository.save(player)
        gameRepository.save(game)

        // when
        gameService.joinGame(player.getPlayerId(), game.getGameId())

        // then
        val captor = ArgumentCaptor.forClass(Binding::class.java)
        verify(rabbitAdmin, times(2)).declareBinding(captor.capture())

        assertThat(captor.allValues)
            .map(Binding::getRoutingKey)
            .containsExactlyInAnyOrder(Tuple.tuple(player.getPlayerId().toString()), Tuple.tuple("public"))
    }

    @Test
    fun shouldPersistGameWithUpdatedMaximumNumberOfRounds() {
        // given
        val game = Game(1,3)
        game.startGame()
        val newMax = 3
        gameRepository.save(game)

        // when
        gameService.changeMaximumNumberOfRounds(game.getGameId(), newMax)

        // then
        val capturedGame: Game = gameRepository.findById(game.getGameId()).get()
        assertThat(capturedGame.getMaxRounds())
            .isEqualTo(newMax)
    }
}
