package microservice.dungeon.game.aggregates.game.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Duration
import java.time.temporal.ChronoUnit

class TimeFrameTest {

    @Test
    fun shouldAllowToCalculateCommandInputTimeFrame() {
        // given
        val timeFrame = TimeFrame(Duration.of(60, ChronoUnit.SECONDS))

        // when
        val commandInputTime = timeFrame.commandInputTime()

        // then
        assertThat(commandInputTime)
            .isEqualByComparingTo(Duration.of(54, ChronoUnit.SECONDS))
    }

    @Test
    fun shouldAllowToExecutionTimeFrame() {
        // given
        val timeFrame = TimeFrame(Duration.of(60, ChronoUnit.SECONDS))

        // when
        val commandInputTime = timeFrame.executionTime()

        // then
        assertThat(commandInputTime)
            .isEqualByComparingTo(Duration.of(6, ChronoUnit.SECONDS))
    }
}