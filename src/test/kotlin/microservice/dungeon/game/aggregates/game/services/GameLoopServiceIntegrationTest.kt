package microservice.dungeon.game.aggregates.game.services

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.eventpublisher.EventPublisher
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameAlreadyActiveException
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.game.events.GameStatusEvent
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert.assertThrows
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.argumentCaptor
import org.mockito.kotlin.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import javax.transaction.Transactional

// I'm sticking to a high-level integration test because I want to rewrite the component anyway
@Transactional
internal class GameLoopServiceIntegrationTest : AbstractIntegrationTest() {
    @Autowired
    lateinit var gameLoopService: GameLoopService

    @Autowired
    lateinit var gameRepository: GameRepository

    // It is relatively hard to test the event publication, so I'm just mocking it.
    @MockBean
    lateinit var eventPublisher: EventPublisher

    @AfterEach
    fun cleanup() {
        gameRepository.deleteAll()
    }


    // Requirement: Only one Game at a time
    @Test
    fun shouldThrowWhenAnotherGameIsAlreadyStarted() {
        val anAlreadyStartedGame = Game(10, 10)
        anAlreadyStartedGame.startGame()
        gameRepository.save(anAlreadyStartedGame)

        val anotherGame = Game(10, 10)
        gameRepository.save(anotherGame)
        assertThrows(GameAlreadyActiveException::class.java) {
            gameLoopService.start(anotherGame.getGameId())
        }
    }

    @Test
    fun shouldStartGame() {
        val aGame = Game(10, 10)
        gameRepository.save(aGame)

        gameLoopService.start(aGame.getGameId())

        val game = gameRepository.findById(aGame.getGameId()).get()
        assertThat(game.getGameStatus())
            .isEqualTo(GameStatus.GAME_RUNNING)
    }

    @Test
    fun shouldPublishGameStartedEvent() {
        val aGame = Game(10, 10)
        gameRepository.save(aGame)

        gameLoopService.start(aGame.getGameId())

        val eventCaptor = argumentCaptor<GameStatusEvent>()
        verify(eventPublisher).publishEvent(eventCaptor.capture())

        assertThat(eventCaptor.firstValue)
            .extracting("gameId", "gameStatus")
            .containsExactly(aGame.getGameId(), GameStatus.GAME_RUNNING)
    }

    @Test
    fun shouldEndGame() {
        val aGame = Game(10, 10)
        aGame.startGame()
        gameRepository.save(aGame)

        gameLoopService.end(aGame.getGameId())

        val game = gameRepository.findById(aGame.getGameId()).get()
        assertThat(game.getGameStatus())
            .isEqualTo(GameStatus.GAME_FINISHED)
    }

    @Test
    fun shouldPublishGameEndedEvent() {
        val aGame = Game(10, 10)
        aGame.startGame()
        gameRepository.save(aGame)

        gameLoopService.end(aGame.getGameId())

        val eventCaptor = argumentCaptor<GameStatusEvent>()
        verify(eventPublisher).publishEvent(eventCaptor.capture())

        assertThat(eventCaptor.firstValue)
            .extracting("gameId", "gameStatus")
            .containsExactly(aGame.getGameId(), GameStatus.GAME_FINISHED)
    }

    @Test
    fun shouldChangeRoundDuration() {
        val aGame = Game(10, 10)
        gameRepository.save(aGame)
        val newDuration = 2024L

        gameLoopService.changeRoundDuration(aGame.getGameId(), newDuration)

        val game = gameRepository.findById(aGame.getGameId()).get()
        assertThat(game.getTotalRoundTimespanInMS())
            .isEqualTo(newDuration)
    }
}