package microservice.dungeon.game.aggregates.game.controller

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.core.GlobalExceptionHandler
import microservice.dungeon.game.aggregates.game.controller.dto.*
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.player.services.PlayerQueueManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody
import org.springframework.test.web.reactive.server.expectBodyList
import java.util.*

class GameControllerIntegrationTest : AbstractIntegrationTest() {
    @Autowired
    lateinit var gameRepository: GameRepository
    @Autowired
    lateinit var playerRepository: PlayerRepository
    @Autowired
    lateinit var gameController: GameController
    @Autowired
    lateinit var globalExceptionHandler: GlobalExceptionHandler
    @Autowired
    lateinit var playerQueueManager: PlayerQueueManager

    lateinit var webTestClient: WebTestClient

    @AfterEach
    fun cleanup() {
        gameRepository.deleteAll()
        playerRepository.deleteAll()
        playerQueueManager.setupGameExchange()
    }

    @BeforeEach
    fun setUp() {
        gameRepository.deleteAll()
        playerRepository.deleteAll()
        webTestClient = WebTestClient
            .bindToController(gameController)
            .controllerAdvice(globalExceptionHandler)
            .build()
    }

    @Test
    fun shouldAllowToCreateNewGame() {
        // given
        val maxPlayers = 3
        val maxRounds = 100
        val requestBody = CreateGameRequestDto(maxPlayers, maxRounds)

        // when then
        val result = webTestClient.post().uri("/games")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(requestBody)
            .exchange()
            .expectStatus().isCreated
            .expectBody<CreateGameResponseDto>()
            .returnResult()
        val responseBody = result.responseBody!!

        val gameId = responseBody.gameId
        val game = gameRepository.findById(gameId)
        assertThat(game)
            .isPresent
            .get()
            .extracting(Game::getGameStatus, Game::getMaxPlayers, Game::getMaxRounds)
            .containsExactly(GameStatus.CREATED, maxPlayers, maxRounds)
    }

    @Test
    fun shouldAllowToStartGame() {
        // given)
        val game = Game(1,10)
        gameRepository.save(game)

        // when then
        webTestClient.post().uri("/games/${game.getGameId()}/gameCommands/start")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isCreated
    }

    @Test
    fun shouldRespondNotFoundWhenGameNotExistsWhileGameStarting() {
        // given
        val game = UUID.randomUUID()

        // when then
        webTestClient.post().uri("/games/$game/gameCommands/start")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
    }

    @Test
    fun shouldRespondBadRequestWhenActionIsNotAllowedWhileGameStarting() {
        // given
        val game = createGame()
        game.startGame()
        gameRepository.save(game)

        // when then
        webTestClient.post().uri("/games/${game.getGameId()}/gameCommands/start")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest
    }

    @Test
    fun shouldAllowToEndGame() {
        // given
        val game = createGame()
        game.startGame()
        gameRepository.save(game)

        // when then
        webTestClient.post().uri("/games/${game.getGameId()}/gameCommands/end")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isCreated
    }

    @Test
    fun shouldRespondNotFoundWhenGameNotExistsWhileGameEnding() {
        // given
        val gameId: UUID = UUID.randomUUID()

        // when then
        webTestClient.post().uri("/games/${gameId}/gameCommands/end")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
    }

    @Test
    fun shouldRespondBadRequestWhenActionIsNotAllowedWhileGameEnding() {
        // given
        val game = createGame()
        gameRepository.save(game)

        // when then
        webTestClient.post().uri("/games/${game.getGameId()}/gameCommands/end")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest
    }

    @Test
    fun shouldAllowPlayerToJoinTheGame() {
        // given
        val game = createGame()
        gameRepository.save(game)

        val player = createPlayer()
        playerRepository.save(player)

        // when then
        val result = webTestClient.put().uri("/games/${game.getGameId()}/players/${player.getPlayerId()}")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody<JoinGameResponseDto>()
            .returnResult()
        val responseBody: JoinGameResponseDto = result.responseBody!!

        // and then
        assertThat(responseBody.gameExchange)
            .isEqualTo("game")
        assertThat(responseBody.playerQueue)
            .isEqualTo("player-${player.getPlayerId()}")
    }

    @Test
    fun shouldRespondNotFoundWhenGameNotExistsWhileJoiningAGame() {
        // given
        val gameId = UUID.randomUUID()
        val playerToken = UUID.randomUUID()

        // when
        webTestClient.put().uri("/games/${gameId}/players/${playerToken}")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
    }

    @Test
    fun shouldRespondNotFoundWhenPlayerNotExistsWhileJoiningAGame() {
        // given
        val gameId = UUID.randomUUID()
        val playerToken = UUID.randomUUID()

        // when
        webTestClient.put().uri("/games/${gameId}/players/${playerToken}")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
    }

    @Test
    fun shouldRespondBadRequestWhenActionIsNotAllowedWhileJoiningAGame() {
        // given
        val game = createGame()
        game.startGame()
        val player = createPlayer()

        playerRepository.save(player)
        gameRepository.save(game)

        // when
        webTestClient.put().uri("/games/${game.getGameId()}/players/${player.getPlayerId()}")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isBadRequest
    }

    @Test
    fun shouldAllowToFetchCurrentGameTime() {
        // given
        val game = createGame()
        game.startGame()
        gameRepository.save(game)

        // when
        val result = webTestClient.get().uri("/games/${game.getGameId()}/time")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody<GameTimeResponseDto>()
            .returnResult()
        val responseBody: GameTimeResponseDto = result.responseBody!!

        // then
        assertThat(responseBody.gameTime)
            .isNotNull
        assertThat(responseBody.roundCount)
            .isEqualTo(1)
        assertThat(responseBody.roundTime)
            .isNotNull
    }

    @Test
    fun shouldRespondNotFoundWhenGameNotExistsWhileFetchingGameTime() {
        // given
        val anyGameId = UUID.randomUUID()

        // when
        webTestClient.get().uri("/games/${anyGameId}/time")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
    }

    @Test
    fun shouldAllowToRetrieveAllActiveGames() {
        // given
        val game = createGame()
        gameRepository.save(game)

        // when
        val result = webTestClient.get().uri("/games")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody<List<GameResponseDto>>()
            .returnResult()
        val responseBodies: List<GameResponseDto> = result.responseBody!!

        // then
        assertThat(responseBodies)
            .hasSize(1)
        assertThat(responseBodies[0])
            .isEqualTo(GameResponseDto(game))
    }

    @Test
    fun shouldAllowToPatchMaximumNumberOfRounds() {
        // given
        val game = createGame()
        gameRepository.save(game)
        val maxRoundDto = PatchGameMaxRoundsDto(5)

        // when
        webTestClient.patch().uri("/games/${game.getGameId()}/maxRounds")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(maxRoundDto)
            .exchange()
            .expectStatus().isOk
    }

    @Test
    fun shouldRespondNotFoundWhenGameNotExistsWhileTryingToChangeMaximumNumberOfRounds() {
        // given
        val gameId = UUID.randomUUID()
        val maxRoundDto = PatchGameMaxRoundsDto(5)

        // when
        webTestClient.patch().uri("/games/${gameId}/maxRounds")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(maxRoundDto)
            .exchange()
            .expectStatus().isNotFound
    }

    @Test
    fun shouldAllowToPatchRoundDuration() {
        // given
        val game = createGame()
        gameRepository.save(game)
        val durationDto = PatchGameDurationDto(3000)

        // when
        webTestClient.patch().uri("/games/${game.getGameId()}/duration")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(durationDto)
            .exchange()
            .expectStatus().isOk
    }

    @Test
    fun shouldRespondNotFoundWhenCatchingGameNotFoundExceptionWhenTryingToPatchRoundDuration() {
        // given
        val gameId = UUID.randomUUID()
        val durationDto = PatchGameDurationDto(3000)

        // when
        webTestClient.patch().uri("/games/${gameId}/duration")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(durationDto)
            .exchange()
            .expectStatus().isNotFound
    }

    @Test
    fun shouldRespondBadRequestWhenCatchingIllegalArgumentExceptionWhenTryingToPatchRoundDuration() {
        // given
        val game = createGame()
        gameRepository.save(game)
        val durationDto = PatchGameDurationDto(-1)

        // when
        webTestClient.patch().uri("/games/${game.getGameId()}/duration")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(durationDto)
            .exchange()
            .expectStatus().isBadRequest
    }

    @Test
    fun shouldNotReturnFinishedGame() {
        // given
        val endedGame = createGame()
        endedGame.startGame()
        endedGame.endGame()
        val otherGame = createGame()
        gameRepository.save(otherGame)
        gameRepository.save(endedGame)

        // when
        val response = webTestClient.get().uri("/games")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBodyList<GameResponseDto>()
            .returnResult()
            .responseBody

        assertThat(response)
            .noneMatch { it.gameId == endedGame.getGameId() }
            .noneMatch { it.gameStatus == "ended" }
    }

    private fun createGame(): Game {
        return Game(10, 10)
    }

    private fun createPlayer(): Player {
        return Player("test-${UUID.randomUUID()}", "test-${UUID.randomUUID()}@example.com")
    }
}
