package microservice.dungeon.game.aggregates.player.controller

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.player.controller.dtos.CreatePlayerRequestDto
import microservice.dungeon.game.aggregates.player.controller.dtos.PlayerResponseDto
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody
import java.util.*

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class PlayerControllerIntegrationTest : AbstractIntegrationTest() {
    @Autowired
    private lateinit var playerRepository: PlayerRepository
    @Autowired
    private lateinit var playerController: PlayerController

    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun setUp() {
        webTestClient = WebTestClient.bindToController(playerController).build()
    }

    @AfterEach
    fun cleanup() {
        playerRepository.deleteAll()
    }


    @Test
    fun shouldAllowToCreateNewPlayer() {
        // given
        val requestEntity = PlayerResponseDto(UUID.randomUUID(), "SOME_NAME", "SOME_MAIL")

        // when
        val result = webTestClient.post().uri("/players")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(requestEntity)
            .exchange()
            .expectStatus().isCreated
            .expectBody<PlayerResponseDto>()
            .returnResult()
        val responseBody = result.responseBody!!

        // then
        assertThat(responseBody.name)
            .isEqualTo(requestEntity.name)
        assertThat(responseBody.email)
            .isEqualTo(requestEntity.email)
        assertThat(responseBody.playerId)
            .isNotNull
    }

    @Test
    fun shouldRespondBadRequestWhenPlayerAlreadyExistsWhileCreatingNew() {
        // given
        val userName = "dadepu"
        val userMail = "dadepu@smail.th-koeln.de"
        val requestEntity = CreatePlayerRequestDto(userName, userMail)
        val existingPlayer = Player(userName, userMail)
        playerRepository.save(existingPlayer)

        // when
        webTestClient.post().uri("/players")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(requestEntity)
            .exchange()
            .expectStatus().isBadRequest
    }

    @Test
    fun shouldAllowToFetchPlayerDetails() {
        // given
        val userName = "dadepu"
        val userMail = "dadepu@smail.th-koeln.de"
        val existingPlayer = Player(userName, userMail)
        playerRepository.save(existingPlayer)

        // when
        val result = webTestClient.get().uri("/players?name=${userName}&mail=${userMail}")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody<PlayerResponseDto>()
            .returnResult()
        val responseBody = result.responseBody!!

        // then
        assertThat(responseBody.name)
            .isEqualTo(existingPlayer.getUserName())
        assertThat(responseBody.email)
            .isEqualTo(existingPlayer.getMailAddress())
        assertThat(responseBody.playerId)
            .isEqualTo(existingPlayer.getPlayerId())
    }

    @Test
    fun shouldRespondNotFoundWhenPlayerNotFoundWhileTryingToFetchDetails() {
        // given
        val userName = "dadepu"
        val userMail = "dadepu@smail.th-koeln.de"

        // when
        webTestClient.get().uri("/players?name=${userName}&mail=${userMail}")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
    }
}
