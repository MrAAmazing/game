package microservice.dungeon.game.aggregates.command.services

import microservice.dungeon.game.aggregates.command.domain.CommandType
import microservice.dungeon.game.aggregates.command.repositories.CommandRepository
import microservice.dungeon.game.aggregates.command.web.RobotClient
import microservice.dungeon.game.aggregates.command.web.TradingClient
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import java.util.*

class CommandServiceTest {

    val tradingClient = mock<TradingClient>()
    val robotClient = mock<RobotClient>()
    val commandRepository = mock<CommandRepository>()
    val robotRepository = mock<RobotRepository>()
    val gameRepository = mock<GameRepository>()
    val playerRepository = mock<PlayerRepository>()
    val commandService = CommandService(
        commandRepository,
        robotRepository,
        gameRepository,
        playerRepository,
        robotClient,
        tradingClient
    )

    @Test
    fun shouldCallTradingServiceForBuyingCommands() {
        commandService.dispatchCommands(UUID.randomUUID(), CommandType.BUYING)
        verify(tradingClient).sendBuyingCommands(any())
    }

    @Test
    fun shouldCallTradingServiceForSellingCommands() {
        commandService.dispatchCommands(UUID.randomUUID(), CommandType.SELLING)
        verify(tradingClient).sendSellingCommands(any())
    }

    @ParameterizedTest
    @EnumSource(value = CommandType::class, names = ["MINING", "MOVEMENT", "BATTLE", "REGENERATE"])
    fun shouldCallRobotServiceForRobotCommands(type: CommandType) {
        commandService.dispatchCommands(UUID.randomUUID(), type)
        verify(robotClient).sendCommands(any())
    }
}