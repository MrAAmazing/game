package microservice.dungeon.game.aggregates.command.services

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.command.controller.dto.CommandObjectRequestDto
import microservice.dungeon.game.aggregates.command.controller.dto.CommandRequestDto
import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.domain.CommandPayload
import microservice.dungeon.game.aggregates.command.domain.CommandType
import microservice.dungeon.game.aggregates.command.repositories.CommandRepository
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.Round
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.game.repositories.RoundRepository
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.domain.Robot
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.DirtiesContext
import java.util.*

@DirtiesContext
class CommandServiceIntegrationTest @Autowired constructor(
    private val commandService: CommandService,
    private val commandRepository: CommandRepository,
    private val playerRepository: PlayerRepository,
    private val roundRepository: RoundRepository,
    private val gameRepository: GameRepository,
    private val robotRepository: RobotRepository
) : AbstractIntegrationTest(){
    private var player: Player? = null
    private var game: Game? = null
    private var round: Round? = null
    private var robot: Robot? = null

    @AfterEach
    fun cleanUp() {
        commandRepository.deleteAll()
        roundRepository.deleteAll()
        gameRepository.deleteAll()
        robotRepository.deleteAll()
        playerRepository.deleteAll()
    }

    @BeforeEach
    fun setUp() {
        player = Player("dadepu", "dadepu@smail.th-koeln.de")
        robot = Robot(UUID.randomUUID(), player!!.getPlayerId())
        playerRepository.save(player!!)
        robotRepository.save(robot!!)

        game = Game(1,10)
        game!!.joinGame(player!!)
        game!!.startGame()
        game!!.startNewRound()
        round = game!!.getCurrentRound()
        gameRepository.save(game!!)
    }

    @Test
    fun shouldPersistCommandWhenCreatingNewCommand() {
        // given
        val requestBody = CommandRequestDto(
            gameId = game!!.getGameId(), playerId = player!!.getPlayerId(), null, CommandType.SELLING,
            CommandObjectRequestDto(
                CommandType.SELLING, null, null, "ROBOT", 1
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            requestBody.gameId, requestBody.playerId, null, CommandType.SELLING, requestBody
        )

        // then
        val command: Command = commandRepository.findById(commandId).get()
        assertThat(command.getCommandType())
            .isEqualTo(CommandType.SELLING)
    }

    @Test
    @Disabled("Don't know if this makes sense")
    fun shouldDeleteDuplicateRobotNulls() {
        // given
        val previousCommand = Command(UUID.randomUUID(), round!!, player!!, null, CommandType.BUYING,
            CommandPayload(null, null, "ROBOT", 1)
        )
        commandRepository.save(previousCommand)

        val newCommandRequest = CommandRequestDto(
            gameId = game!!.getGameId(), playerId = player!!.getPlayerId(), null, CommandType.SELLING,
            CommandObjectRequestDto(
                CommandType.SELLING, null, null, "ROBOT", 1
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            newCommandRequest.gameId, newCommandRequest.playerId, newCommandRequest.robotId, CommandType.SELLING, newCommandRequest
        )

        // then
        val existsPreviousCommand = commandRepository.existsById(previousCommand.getCommandId())
        val existsNewCommand = commandRepository.existsById(commandId)

        assertThat(existsPreviousCommand)
            .isFalse
        assertThat(existsNewCommand)
            .isTrue
    }

    @Test
    fun shouldDeleteDuplicates() {
        // given
        val previousCommand = Command(UUID.randomUUID(), round!!, player!!, robot!!, CommandType.SELLING,
            CommandPayload(null, null, "ROBOT", 1)
        )
        commandRepository.save(previousCommand)

        val newCommandRequest = CommandRequestDto(
            gameId = game!!.getGameId(), playerId = player!!.getPlayerId(), robot!!.getRobotId(), CommandType.SELLING,
            CommandObjectRequestDto(
                CommandType.SELLING, null, null, "ROBOT", 1
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            newCommandRequest.gameId, newCommandRequest.playerId, newCommandRequest.robotId, CommandType.SELLING, newCommandRequest
        )

        // then
        val existsPreviousCommand = commandRepository.existsById(previousCommand.getCommandId())
        val existsNewCommand = commandRepository.existsById(commandId)

        assertThat(existsPreviousCommand)
            .isFalse
        assertThat(existsNewCommand)
            .isTrue
    }

    @Test
    fun shouldNotDeleteConsecutiveRobotNullCommands() {
        // given
        val previousCommand = Command(UUID.randomUUID(), round!!, player!!, null, CommandType.BUYING,
            CommandPayload(null, null, "ROBOT", 1)
        )
        commandRepository.save(previousCommand)

        val newCommandRequest = CommandRequestDto(
            gameId = game!!.getGameId(), playerId = player!!.getPlayerId(), robot!!.getRobotId(), CommandType.SELLING,
            CommandObjectRequestDto(
                CommandType.SELLING, null, null, "ROBOT", 1
            )
        )

        // when
        val commandId: UUID = commandService.createNewCommand(
            newCommandRequest.gameId, newCommandRequest.playerId, newCommandRequest.robotId, CommandType.SELLING, newCommandRequest
        )

        // then
        val existsPreviousCommand = commandRepository.existsById(previousCommand.getCommandId())
        val existsNewCommand = commandRepository.existsById(commandId)

        assertThat(existsPreviousCommand)
            .isTrue
        assertThat(existsNewCommand)
            .isTrue
    }

    // create new commands
    // remove duplicates
    // find commands by id
}
