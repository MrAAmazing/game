package microservice.dungeon.game.aggregates.command.controller

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.command.controller.dto.CommandObjectRequestDto
import microservice.dungeon.game.aggregates.command.controller.dto.CommandRequestDto
import microservice.dungeon.game.aggregates.command.controller.dto.CommandResponseDto
import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.command.domain.CommandType
import microservice.dungeon.game.aggregates.command.repositories.CommandRepository
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.game.repositories.RoundRepository
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.domain.Robot
import microservice.dungeon.game.aggregates.robot.domain.RobotStatus
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody
import java.util.*

@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class CommandControllerIntegrationTest : AbstractIntegrationTest() {
    @Autowired
    private lateinit var playerRepository: PlayerRepository
    @Autowired
    private lateinit var gameRepository: GameRepository
    @Autowired
    private lateinit var roundRepository: RoundRepository
    @Autowired
    private lateinit var robotRepository: RobotRepository
    @Autowired
    private lateinit var commandRepository: CommandRepository
    @Autowired
    private lateinit var commandController: CommandController
    @Autowired
    private lateinit var commandControllerExceptionHandler: CommandExceptionHandler

    private lateinit var webTestClient: WebTestClient

    private lateinit var testPlayer: Player
    private lateinit var testGame: Game
    private lateinit var testRobot: Robot


    @BeforeEach
    fun setUp() {
        testPlayer = playerRepository.save(Player("SOME_NAME", "SOME_MAIL"))
        testGame = gameRepository.save(Game(10, 10))
        testRobot = robotRepository.save(Robot(UUID.randomUUID(), testPlayer.getPlayerId(), RobotStatus.ACTIVE))

        webTestClient = WebTestClient
            .bindToController(commandController)
            .controllerAdvice(commandControllerExceptionHandler)
            .build()
    }

    @AfterEach
    fun tearDown() {
        commandRepository.deleteAll()
        robotRepository.deleteAll()
        gameRepository.deleteAll()
        playerRepository.deleteAll()
        roundRepository.deleteAll()
    }

    private fun createCommandDto(type: CommandType = CommandType.REGENERATE): CommandRequestDto {
        return CommandRequestDto(
            testGame.getGameId(), testPlayer.getPlayerId(), testRobot.getRobotId(), type, CommandObjectRequestDto(
                type, null, null, null, null
            )
        )
    }

    @Test
    fun shouldAllowToOverwriteCommands() {
        // given
        testGame.joinGame(testPlayer)
        testGame.startGame()
        gameRepository.save(testGame)

        val existingCommand = Command(
            round = testGame.getCurrentRound()!!,
            player = testPlayer,
            robot = testRobot,
            commandType = CommandType.MINING,
            commandPayload = null
        );
        commandRepository.save(existingCommand)

        val newCommand = createCommandDto()

        // when
        webTestClient.post().uri("/commands")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(newCommand)
            .exchange()
            .expectStatus().isCreated
            .expectBody<CommandResponseDto>()
            .returnResult()

        val commands = commandRepository.findAllCommandsByRound(testGame.getCurrentRound()!!)
        assertThat(commands)
            .hasSize(1)
            .first()
            .extracting("commandType")
            .isEqualTo(newCommand.commandType)
    }

    @Test
    fun shouldAllowToCreateCommands() {
        // given
        testGame.joinGame(testPlayer)
        testGame.startGame()
        gameRepository.save(testGame)
        val requestBody = createCommandDto()

        // when
        webTestClient.post().uri("/commands")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(requestBody)
            .exchange()
            .expectStatus().isCreated
            .expectBody<CommandResponseDto>()
            .returnResult()
    }

    @Test
    fun shouldFailOnTypo() {
        // when
        webTestClient.post().uri("/commands")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue("""
                {
                  "gameId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                  "playerId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                  "robotId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                  "commandType": "battle",
                  "commandObject": {
                    "commandType": "battle",
                    "planetId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                    "targetId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                    "itemName": "string",
                    "itemQuantitiy": 5
                  }
                }
            """.trimIndent())
            .exchange()
            .expectStatus().is4xxClientError
    }

    @Test
    fun shouldFailOnAdditionalProperty() {
        // when
        webTestClient.post().uri("/commands")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue("""
                {
                  "gameId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                  "playerId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                  "robotId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                  "roundId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                  "commandType": "battle",
                  "commandObject": {
                    "commandType": "battle",
                    "planetId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                    "targetId": "d290f1ee-6c54-4b01-90e6-d701748f0851",
                    "itemName": "string",
                    "itemQuantity": 5
                  }
                }
            """.trimIndent())
            .exchange()
            .expectStatus().is4xxClientError
    }

    @Test
    fun shouldPreventToCreateCommandsWhenGameIsNotStarted() {
        // given
        val requestBody = createCommandDto()

        // when
        webTestClient.post().uri("/commands")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(requestBody)
            .exchange()
            .expectStatus().isBadRequest
    }

    @Test
    fun shouldPreventToCreateCommandsWhenPlayerIsNotParticipating() {
        // given
        testGame.startGame()
        gameRepository.save(testGame)
        val requestBody = createCommandDto()

        // when
        webTestClient.post().uri("/commands")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .bodyValue(requestBody)
            .exchange()
            .expectStatus().isBadRequest
    }
}
