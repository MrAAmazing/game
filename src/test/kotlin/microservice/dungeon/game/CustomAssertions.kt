package microservice.dungeon.game

import microservice.dungeon.game.aggregates.player.domain.Player
import org.assertj.core.api.Assertions

class CustomAssertions: Assertions() {
    companion object {
        fun assertThat(actual: Player) = PlayerAssertion(actual)
        fun assertThat(actual: RestProducerContract) = RestProducerContractAssertion(actual)
    }
}