[![pipeline status](https://gitlab.com/the-microservice-dungeon/game/badges/main/pipeline.svg)](https://gitlab.com/the-microservice-dungeon/game/-/commits/main) |
[![coverage report](https://gitlab.com/the-microservice-dungeon/game/badges/main/coverage.svg)](https://gitlab.com/the-microservice-dungeon/game/-/commits/main) |
[Test Coverage Report](https://gitlab.com/the-microservice-dungeon/game/-/jobs/artifacts/main/file/target/site/jacoco/index.html?job=test:jdk17) |
[Code Quality Report](https://the-microservice-dungeon.gitlab.io/game)

___

# Game

## Development

The easiest way to start development is by using the [local-dev-env](https://gitlab.com/the-microservice-dungeon/local-dev-environment). 
Which provides you with a full environment to develop the game service.
However, the local-dev-environment will also start a game service by itself which blocks the port.
Therefore, make sure to don't start the game service by following the instructions in the local-dev-env 
documentation. 
